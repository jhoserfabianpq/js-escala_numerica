
/**
* @description Muestra la escala numerica en forma de tabla
* @var {string} rta1 Se le es asignado la funcion crearOperacion(int, int), en el cual realiza la multiplicacion
* @var {string} rta2 Se le es asignado la funcion crearOperacion(int, int), en el cual realiza la division
*/
function crearEscala() {

  var numero = document.getElementById("entrada").value;
  var rta1 = crearOperacion(0, numero);
  var rta2 = crearOperacion(1, rta1[0]);

  var tabla = "";
  tabla +=
    `
    <table class='center' style='width:50%;'>
      <tr>
        <th></th>
        <th>Escala del numero: ${numero}</th>
        <th></th>
      </tr>
      <tr>
        <th> Dato 1 </th>
        <th> Dato 2 </th>
        <th> Resultado </th>
      </tr>
      <tr>
        <th></th>
        <th> Operacion Multiplicar </th>
        <th></th>
        ${rta1[1]}
      </tr>
      <tr>
        <th></th>
        <th> Operacion Dividir </th>
        <th></th>
        ${rta2[1]}
      </tr>
    </table>
    `;

  document.getElementById("rta3").innerHTML = tabla;
}

/**
* @description Realiza la operacion respectiva de la escala validada con unasentencia de desicion
* @param {number} operacion recibe un valor numerico, 0 para realizar la multiplicacion y 1 para la divicion
* @param {number} numero el numero digitado para realizar la escala numerica
* @return {Array[number,string]} vectorRta en la [0] almacena el resultado y en [1] almaneca las celdas de la tabla escala
*/
function crearOperacion(operacion, numero) {

  let m = [" * ", " / "];
  var rta = "";
  let vectorRta = new Array(2);
  for (var i = 1; i <= 10; i++) {

    var operacionResultado = 0;
    if (!operacion) operacionResultado = numero * i;
    else operacionResultado = numero / i;

    var mensaje = numero;
    numero = operacionResultado;

    rta +=
      `
    <tr>
      <td>${mensaje}</td>
      <td style="text-align:center">${i}</td>
      <td style="text-align:right">${operacionResultado}</td>
    </tr>
    `;

  }
  vectorRta[0] = numero;
  vectorRta[1] = rta;
  return vectorRta;
}
